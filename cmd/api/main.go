package main

import (
	"context"
	"database/sql"
	"flag"
	"os"
	"sync"
	"time"

	"github.com/BurntSushi/toml"
	_ "github.com/lib/pq"
	"test_service.mjekson.ru/internal/data"
	"test_service.mjekson.ru/internal/jsonlog"
	"test_service.mjekson.ru/internal/mailer"
)

const version = "1.0.0"

type application struct {
	config Config
	logger *jsonlog.Logger
	models data.Models
	mailer mailer.Mailer
	wg     sync.WaitGroup
}

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "../../configs/api.toml", "path to config file") //../../configs/api.toml
}

func main() {
	flag.Parse()

	logger := jsonlog.New(os.Stdout, jsonlog.LevelInfo)

	cfg := NewConfig()

	_, err := toml.DecodeFile(configPath, &cfg)
	if err != nil {
		logger.PrintFatal(err, nil)
	}

	db, err := openDB(cfg)
	if err != nil {
		logger.PrintFatal(err, nil)
	}
	defer db.Close()

	logger.PrintInfo("database connection pool established", nil)

	app := &application{
		config: cfg,
		logger: logger,
		models: data.NewModels(db),
		mailer: mailer.New(
			cfg.Smtp.Host,
			cfg.Smtp.Port,
			cfg.Smtp.Username,
			cfg.Smtp.Password,
			cfg.Smtp.Sender),
	}

	err = app.serve()
	if err != nil {
		logger.PrintFatal(err, nil)
	}

}

func openDB(cfg Config) (*sql.DB, error) {
	db, err := sql.Open("postgres", cfg.Db.Dsn)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(cfg.Db.MaxOpenConns)
	db.SetMaxIdleConns(cfg.Db.MaxIdleConns)

	duration, err := time.ParseDuration(cfg.Db.MaxIdleTime)
	if err != nil {
		return nil, err
	}

	db.SetConnMaxIdleTime(duration)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	return db, nil
}
