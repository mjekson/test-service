package main

type Config struct {
	Port int    `toml:"port"`
	Env  string `toml:"env"`
	Db   struct {
		Dsn          string `toml:"dsn"`
		MaxOpenConns int    `toml:"maxOpenConns"`
		MaxIdleConns int    `toml:"maxIdleConns"`
		MaxIdleTime  string `toml:"maxIdleTime"`
	}
	Limiter struct {
		Rps     float64 `toml:"rps"`
		Burst   int     `toml:"burst"`
		Enabled bool    `toml:"enabled"`
	}
	Smtp struct {
		Host     string `toml:"host"`
		Port     int    `toml:"port"`
		Username string `toml:"username"`
		Password string `toml:"password"`
		Sender   string `toml:"sender"`
	}
}

func NewConfig() Config {
	return Config{}
}
