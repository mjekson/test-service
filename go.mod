module test_service.mjekson.ru

go 1.17

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/go-mail/mail v2.3.1+incompatible // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/lib/pq v1.10.5 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4 // indirect
	golang.org/x/time v0.0.0-20220411224347-583f2d630306 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
